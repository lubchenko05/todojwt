package com.kpi.todojwt.controllers;


import com.kpi.todojwt.models.UserJWT;
import com.kpi.todojwt.services.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class JWTController {

    private final JWTService jwtService;

    @Autowired
    public JWTController(JWTService jwtService) {
        this.jwtService = jwtService;
    }

    @PostMapping("/token/create")
    public ResponseEntity<?> getToken(@RequestBody UserJWT user) {
        try {
            return new ResponseEntity<>(jwtService.getToken(user), HttpStatus.OK);
        } catch (Exception exc) {
            return new ResponseEntity<>("Error, wrong parameters!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/token/{token}")
    public ResponseEntity<?> getUserId(@PathVariable String token) {
        try {
            return new ResponseEntity<>(jwtService.getUser(token), HttpStatus.OK);
        } catch (Exception exc) {
            return new ResponseEntity<>("Invalid token!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<?> requests() {
        return new ResponseEntity<>(
        "<a href=\"/token/eyJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIxMjMiLCJ1c2VybmFtZSI6Inl1cm9ra2syMzEiLCJwYXNzd29yZCI6InBhc3N3b3JkIn0.j2aS30SODTkeXYNchItTThDLqrd_ewo_lY6I8GX21cE\">/token/{token} (GET)</a>" + "<br/>" + "<br/>"
                + "/token/create (POST)"+ "<br/>" + "<br/>" + "*userId"+ "<br/>" + "<br/>" + "*username"+ "<br/>"+ "<br/>" + "*password"+ "<br/>",
                HttpStatus.OK);
    }

}

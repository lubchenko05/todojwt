package com.kpi.todojwt.models;


public class UserJWT {
    public String userId;
    public String username;
    public String password;

    public UserJWT () {}

    public UserJWT(String userId, String username, String password) {
        this.userId = userId;
        this.username = username;
        this.password = password;
    }

}
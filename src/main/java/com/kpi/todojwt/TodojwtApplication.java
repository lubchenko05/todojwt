package com.kpi.todojwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodojwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodojwtApplication.class, args);
	}

}

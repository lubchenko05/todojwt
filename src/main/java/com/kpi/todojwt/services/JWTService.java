package com.kpi.todojwt.services;

import com.kpi.todojwt.models.UserJWT;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
public class JWTService {
    public String getToken(UserJWT user) throws UnsupportedEncodingException {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        byte[] key = "secret".getBytes("UTF-8");

        JwtBuilder builder = Jwts.builder()
                .claim("userId", user.userId)
                .claim("username", user.username)
                .claim("password", user.password)
                .signWith(signatureAlgorithm, key);

        return builder.compact();
    }
    public UserJWT getUser(String token) throws UnsupportedEncodingException {
            Jws<Claims> claims = Jwts.parser().setSigningKey("secret".getBytes("UTF-8")).parseClaimsJws(token);
            UserJWT u = new UserJWT(claims.getBody().get("userId").toString(),
                                    claims.getBody().get("username").toString(),
                                    claims.getBody().get("password").toString());
            return u;
    }
}
